import React, { useEffect, useState } from "react";
import Navbar from "../Navbar/Navbar";
import "./Products.css";
import Card from "react-bootstrap/Card";
import axios from "axios";

function Product() {
  const [modalOpen, setModalOpen] = useState(false);
  const [data, setData] = useState([]);
  const [id, setid] = useState([]);
  const [apiData, setApiData] = useState([]);
  useEffect(() => {
    axios
      .get(
        "https://randomuser.me/api/?inc=gender,name,nat,location,picture,email&results=%2020 "
      )
      .then((res) => {
        setData(res.data.results);
        console.log(res.data.results);
      });
  }, []);

  return (
    <div className="products">
      <div>
        <Navbar />
      </div>
      <div className="content">
        <div
          style={{
            flex: 0.5,
            display: "flex",
            justifyContent: "center",
            height: "50vh",
            paddingBottom: "10px",
          }}
        >
          <div className="popup">
            {modalOpen ? (
              <div
                className="popupModal"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 0.5,
                }}
              >
                <Card
                  onChange={() => setModalOpen(true)}
                  style={{
                    display: "flex",
                    width: "100%",
                    boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
                  }}
                >
                  <Card.Body
                    style={{
                      display: "flex",
                      backgroundColor: "#fff",
                    }}
                  >
                    <Card.Img
                      variant="top"
                      src={apiData.picture.large}
                      style={{
                        borderRadius: "100%",
                        height: "20vh",
                        width: "10vw",
                      }}
                    />
                    <div style={{ backgroundColor: "#fff" }}>
                      <Card.Title style={{ backgroundColor: "#fff" }}>
                        <ul className="popupCardTitle">
                          <li>
                            {apiData.name.title}. {apiData.name.first}{" "}
                            {apiData.name.last}
                          </li>
                        </ul>
                      </Card.Title>
                      <Card.Text
                        style={{
                          backgroundColor: "#fff",
                        }}
                      >
                        <ul
                          className="popupCardList"
                          style={{
                            backgroundColor: "#fff",
                            listStyleType: "none",
                            display: "flex",
                          }}
                        >
                          <li style={{ color: "#a259ff" }}>
                            {apiData.location.street.number},
                          </li>
                          <li>{apiData.location.street.name}, </li>
                          <li>
                            {apiData.location.state}, {apiData.location.city},{" "}
                          </li>
                          <li>{apiData.location.country}, </li>
                          <li>{apiData.location.postcode}</li>
                        </ul>

                        <p
                          style={{
                            marginLeft: "2em",
                            backgroundColor: "#fff",
                          }}
                        >
                          {apiData.location.timezone.offset}{" "}
                          {apiData.location.timezone.description}
                        </p>
                      </Card.Text>
                    </div>
                  </Card.Body>
                </Card>
              </div>
            ) : (
              <div style={{ height: "50vh" }}></div>
            )}
          </div>
        </div>
        <div className="Data">
          <div className="preData">
            {data.map((ele, index) => {
              return (
                <Card
                  key={index}
                  onClick={() => {
                    setModalOpen(true);
                    setid(index);
                    setApiData(ele);
                  }}
                  className={id === index ? "CardBgChange" : "card"}
                >
                  <Card.Body
                    className={
                      id === index ? "CardBgChange" : "cardbody-content"
                    }
                  >
                    <p
                      className={
                        id === index ? "CardBgChange" : "cardbody-content"
                      }
                    >
                      {ele.gender.charAt(0).toUpperCase() + ele.gender.slice(1)}
                      . NL
                    </p>
                    <div
                      className={id === index ? "SelectUserName" : "userName"}
                    >
                      <h4
                        className={
                          id === index ? "SelectUserName1" : "cardbody-content"
                        }
                      >
                        {ele.name.title} {ele.name.first} {ele.name.last}
                      </h4>
                    </div>
                    <p
                      className={
                        id === index ? "CardBgChange" : "cardbody-email"
                      }
                    >
                      {ele.email}
                    </p>
                  </Card.Body>
                </Card>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Product;
