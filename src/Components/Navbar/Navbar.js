import React from "react";
import "./Nav.css";

function Navbar() {
  return (
    <div className="Navbar">
      <div className="logo">
        <h4>YourChallenge</h4>
      </div>

      <div className="navLink">
        <a href="/">
          <p className="link1">Product</p>
        </a>
        <a href="/" className="link2">
          <p>Download</p>
        </a>
        <a href="/" className="link3">
          <p>Price</p>
        </a>
      </div>
    </div>
  );
}

export default Navbar;
